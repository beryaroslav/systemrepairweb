﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SystemRepairWeb.Migrations
{
    public partial class AddAllotments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MonthlyAllotmentID",
                table: "Repair",
                nullable: true
                );

            migrationBuilder.CreateTable(
                name: "MonthlyAllotment",
                columns: table => new
                {
                    MonthlyAllotmentID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Month = table.Column<DateTime>(nullable: false),
                    MonthlyRepairability = table.Column<double>(nullable: false),
                    WorkDays = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MonthlyAllotment", x => x.MonthlyAllotmentID);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Repair_MonthlyAllotment_MonthlyAllotmentID",
                table: "Repair",
                column: "MonthlyAllotmentID",
                principalTable: "MonthlyAllotment",
                principalColumn: "MonthlyAllotmentID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Repair_MonthlyAllotment_MonthlyAllotmentID",
                table: "Repair");

            migrationBuilder.DropTable(
                name: "MonthlyAllotment");

            migrationBuilder.AlterColumn<int>(
                name: "MonthlyAllotmentID",
                table: "Repair",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
