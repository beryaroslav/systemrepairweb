﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace SystemRepairWeb.Models
{
    public class RepairSparePart
    {
        [Required]
        [Key]
        public int RepairSparePartID { get; set; }

        public int RepairID { get; set; }

        public int SparePartID { get; set; }

        public virtual Repair Repair { get; set; }
        public virtual SparePart SparePart { get; set; }
    }
}
