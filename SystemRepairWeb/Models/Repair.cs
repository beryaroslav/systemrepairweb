﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SystemRepairWeb.Models
{
    /// <summary>
    /// Ремонт
    /// </summary>
    public class Repair
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        public int RepairID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата начала")]
        public DateTime DateStart { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата окончания")]
        public DateTime DateFinish { get; set; }
        public int MachineID { get; set; }
        /// <summary>
        /// Ремонтируемое оборудование
        /// </summary>
        public virtual Machine Machine { get; set; }
        public int TypeOfRepairID { get; set; }
        /// <summary>
        /// Тип ремонта
        /// </summary>
        public virtual TypeOfRepair Type { get; set; }

        //TODO: придумать, как привязать, или отвязать полностью
        public int? MonthlyAllotmentID { get; set; }
        public virtual MonthlyAllotment Month { get; set; }
        /// <summary>
        /// Примечание
        /// </summary>
        [StringLength(300)]
        public string Note { get; set; }
        /// <summary>
        /// Использованные масла
        /// </summary>
        public virtual ICollection<RepairOil> Oils { get; set; }
        /// <summary>
        /// Использованные СОЖ
        /// </summary>
        public virtual ICollection<RepairCoolant> Coolants { get; set; }
        /// <summary>
        /// Использованные запчасти
        /// </summary>
        public virtual ICollection<RepairSparePart> SpareParts { get; set; }
    }
}
