﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SystemRepairWeb.Models;
//using System.Data.Entity.ModelConfiguration.Conventions;
namespace SystemRepairWeb.Models
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }


        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLazyLoadingProxies()
                .UseSqlServer(DefaultConnection);
        }*/
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            string adminRoleName = "admin";
            string userRoleName = "user";

            string adminEmail = "admin@mail.ru";
            string adminPassword = "123456";

           // modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            // добавляем роли
            Role adminRole = new Role { RoleID = 1, Name = adminRoleName };
            Role userRole = new Role { RoleID = 2, Name = userRoleName };
            User adminUser = new User { UserID = 1, Email = adminEmail, Password = adminPassword, RoleID = adminRole.RoleID };

            modelBuilder.Entity<Role>().HasData(new Role[] { adminRole, userRole });
            modelBuilder.Entity<User>().HasData(new User[] { adminUser });

            modelBuilder.Entity<Coolant>().ToTable("Coolant");
            modelBuilder.Entity<Oil>().ToTable("Oil");
            modelBuilder.Entity<SparePart>().ToTable("SparePart");

            modelBuilder.Entity<SortOfMachine>().ToTable("SortOfMachine");
            modelBuilder.Entity<GroupOfMachine>().ToTable("GroupOfMachine");
            modelBuilder.Entity<TypeOfMachine>().ToTable("TypeOfMachine");
            modelBuilder.Entity<ModelOfMachine>().ToTable("ModelOfMachine");
            modelBuilder.Entity<Machine>().ToTable("Machine");

            modelBuilder.Entity<Repair>().ToTable("Repair");
            modelBuilder.Entity<MonthlyAllotment>().ToTable("MonthlyAllotment");
            /*modelBuilder.Entity<Repair>()//.ToTable("Repair")
                .HasMany(c=>c.SpareParts)
                .WithMany(p=>p.Repairs)
                .Map(m =>
                {
                    m.ToTable("RepairSparePart");
                    m.MapLeftKey("RepairID");
                    m.MapRightKey("SparePartID");
                });*/
            modelBuilder.Entity<RepairCoolant>().ToTable("RepairCoolant");
            modelBuilder.Entity<RepairCoolant>().
                HasKey(c => new {c.RepairCoolantID, c.SparePartID});
            modelBuilder.Entity<RepairOil>().ToTable("RepairOil");
            modelBuilder.Entity<RepairOil>().
                HasKey(c => new { c.RepairOilID, c.SparePartID });

            modelBuilder.Entity<RepairSparePart>().ToTable("RepairSparePart");
            modelBuilder.Entity<RepairSparePart>().
                HasKey(c => new { c.RepairSparePartID, c.SparePartID });
            modelBuilder.Entity<RepairSparePart>()
                .HasOne(rsp => rsp.Repair)
                .WithMany(r => r.SpareParts)
                .HasForeignKey(rsp => rsp.RepairID);
            modelBuilder.Entity<RepairSparePart>()
                .HasOne(rsp => rsp.SparePart)
                .WithMany(s => s.RepairSpareParts)
                .HasForeignKey(rsp => rsp.SparePartID);



            modelBuilder.Entity<CycleOfRepair>().ToTable("CycleOfRepair");

            modelBuilder.Entity<Workshop>().ToTable("Workshop");

            base.OnModelCreating(modelBuilder);
        }
        public DbSet<GroupOfMachine> GroupOfMachines { get; set; }
        public DbSet<SortOfMachine> SortOfMachines { get; set; }
        public DbSet<Coolant> Coolants { get; set; }
        public DbSet<Oil> Oils { get; set; }
        public DbSet<SparePart> SpareParts { get; set; }
        public DbSet<TypeOfMachine> TypeOfMachines { get; set; }
        public DbSet<ModelOfMachine> ModelOfMachines { get; set; }
        public DbSet<Machine> Machines { get; set; }
        public DbSet<Repair> Repairs { get; set; }
        public DbSet<RepairCoolant> RepairCoolants { get; set; }
        public DbSet<RepairOil> RepairOils { get; set; }
        public DbSet<RepairSparePart> RepairSpareParts { get; set; }
        public DbSet<CycleOfRepair> CycleOfRepairs { get; set; }
        public DbSet<Workshop> Workshops { get; set; }
        public DbSet<MonthlyAllotment> MonthlyAllotments { get; set; }
        public DbSet<SystemRepairWeb.Models.TypeOfRepair> TypeOfRepair { get; set; }
        public DbSet<SystemRepairWeb.Models.MonthlyAllotment> MonthlyAllotment { get; set; }
    }
}
