﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
namespace SystemRepairWeb.Models
{
    /// <summary>
    /// Месяц производственного календаря
    /// </summary>
    public class MonthlyAllotment
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [Required]
        public int MonthlyAllotmentID { get; set; }
        /// <summary>
        /// Месяц
        /// </summary>
        [Required]
        public DateTime Month { get; set; }
        /// <summary>
        /// Месячная ремонтосложность
        /// </summary>
        public double MonthlyRepairability { get; set; }
        /// <summary>
        /// Количество рабочих дней
        /// </summary>
        public int WorkDays { get; set; }
        /// <summary>
        /// Отображающаяся дата
        /// </summary>
        public string Date
        {
            get
            {
                List<String> months = new List<String>
                {
                    "Январь",
                    "Февраль",
                    "Март",
                    "Апрель",
                    "Май",
                    "Июнь",
                    "Июль",
                    "Август",
                    "Сентябрь",
                    "Октябрь",
                    "Ноябрь",
                    "Декабрь"
                };
                return months[Month.Month-1] + " " + Month.Year;
            }
        }
        public double RealMonthRepairability
        {
            get
            {
                double sum = 0;
                foreach(var item in Repairs)
                {
                    sum += item.Machine.Model.Repairability;
                }

                return sum;
            }
        }
        /// <summary>
        /// Ремонты, проводимые в текущий месяц
        /// </summary>
        public virtual ICollection<Repair> Repairs { get; set; }
    }

    
}
