﻿using System;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace SystemRepairWeb.Models
{
    public class ModelOfMachineViewModel
    {

        public List<ModelOfMachine> Models { get; set; }
        public SelectList Types { get; set; }
        public string TypeOfMachine { get; set; }
        public string SearchString { get; set; }

        public SelectList Countries { get; set; }
        public string Country { get; set; }
    }
}
