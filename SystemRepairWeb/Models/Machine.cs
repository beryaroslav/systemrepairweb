﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SystemRepairWeb.Models
{
    /// <summary>
    /// Единица оборудования
    /// </summary>
    public class Machine
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [Required] public int MachineID { get; set;}

        public int ModelOfMachineID { get; set; }
        /// <summary>
        /// Модель оборудования
        /// </summary>
        
        public virtual ModelOfMachine Model { get; set; }

        public int WorkshopID { get; set; }
        /// <summary>
        /// Цех
        /// </summary>
        public virtual Workshop Workshop { get; set; }
        [Required]
        public string InventoryNumber { get; set; }
        /// <summary>
        /// Дата выпуска
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата выпуска")]
        public DateTime DateStart { get; set; }
        public virtual ICollection<Repair> Repairs { get; set; }
        // TODO: изучить, почему при попытке вывода Model.Name класс не создается и вылетает ошибка
        public string NameNumber
        {
            get { return String.Format("{0} и/н {1} ", Model.Name, InventoryNumber); }
        }
    }
}
