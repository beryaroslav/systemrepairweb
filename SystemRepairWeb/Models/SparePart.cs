﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SystemRepairWeb.Models
{
    /// <summary>
    /// Запасная часть
    /// </summary>
    public class SparePart
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [Required]
        public int SparePartID { get; set; }
        /// <summary>
        /// Название
        /// </summary>
        [Required]
        public string Name { get; set; }

        public virtual ICollection<RepairSparePart> RepairSpareParts { get; set; }
    }
}
