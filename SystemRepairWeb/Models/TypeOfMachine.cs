﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SystemRepairWeb.Models
{
    /// <summary>
    /// Тип оборудования
    /// </summary>
    public class TypeOfMachine
    {
        [Key]
        [Required]
        public int TypeOfMachineID { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }
        public int GroupOfMachineID { get; set; }
        /// <summary>
        /// Группа оборудования
        /// </summary>
        //[Required]
        public virtual GroupOfMachine Group { get; set; }

        public virtual ICollection<ModelOfMachine> Models { get; set; }
    }
}
