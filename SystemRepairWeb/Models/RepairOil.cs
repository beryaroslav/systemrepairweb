﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SystemRepairWeb.Models
{
    public class RepairOil
    {
        [Required]
        [Key]
        public int RepairOilID { get; set; }

        public int RepairID { get; set; }

        public int SparePartID { get; set; }

        public virtual Repair Repair { get; set; }
        public virtual SparePart SparePart { get; set; }
    }
}
