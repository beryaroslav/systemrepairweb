﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SystemRepairWeb.Models
{
    /// <summary>
    /// Тип ремонта
    /// </summary>
    public class TypeOfRepair
    {
        [Key]
        [Required]
        public int TypeOfRepairID { get; set; }
        /// <summary>
        /// Название 
        /// </summary>
        [Required]
        public string Name { get; set; }
       // public string Abbreviation { get; set; }
        public virtual ICollection<CycleOfRepair> Cycles { get; set; }

        public virtual ICollection<Repair> Repairs { get; set; }
    }
}
