﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SystemRepairWeb.Models
{
    /// <summary>
    /// Масло
    /// </summary>
    public class Oil
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [Required]
        public int OilID { get; set; }
        /// <summary>
        /// Название
        /// </summary>
        [Required]
        public string Name;

        public virtual ICollection<RepairOil> RepairOils { get; set; }

    }
}
