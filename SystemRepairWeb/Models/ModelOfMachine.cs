﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
namespace SystemRepairWeb.Models
{
    /// <summary>
    /// Модель оборудования
    /// </summary>
    public class ModelOfMachine
    {
        [Key]
        [Required]
        public int ModelOfMachineID { get; set; }
        /// <summary>
        /// Название
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int TypeOfMachineID { get; set; }
        /// <summary>
        /// Тип оборудования
        /// </summary>
      //  [Required]
        public virtual TypeOfMachine Type { get; set; }
        /// <summary>
        /// Модель ЧПУ
        /// </summary>
        public string ModelCNC { get; set; }
        /// <summary>
        /// Ремонтосложность
        /// </summary>
        public float Repairability { get; set; }
        /// <summary>
        /// Страна производитель
        /// </summary>
        [StringLength(50)]
        public string Country { get; set; }

        public virtual ICollection<Machine> Machines { get; set; }
    }
}
