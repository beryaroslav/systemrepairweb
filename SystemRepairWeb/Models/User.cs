﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystemRepairWeb.Models
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User
    {
        public int UserID { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public int? RoleID { get; set; }
        public virtual Role Role { get; set; }
    }
}
