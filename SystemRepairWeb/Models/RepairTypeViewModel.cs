﻿
using System;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
namespace SystemRepairWeb.Models
{
    public class RepairTypeViewModel
    {
        public List<Repair> Repairs{ get; set; }
        public SelectList Types { get; set; }
        public string TypeOfRepair { get; set; }
        public string SearchString { get; set; }

        public SelectList Workshops { get; set; }
        public string Workshop { get; set; }
        //public DateTime Start { get; set; }

        //public SelectList 
        //public DateTime End { get; set; }
    }
}
