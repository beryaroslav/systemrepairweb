﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SystemRepairWeb.Models
{
    public class RepairCoolant
    {
        [Required]
        [Key]
        public int RepairCoolantID { get; set; }

        public int RepairID { get; set; }

        public int SparePartID { get; set; }

        public virtual Repair Repair { get; set; }
        public virtual SparePart SparePart { get; set; }
    }
}
