﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SystemRepairWeb.Models
{
    /// <summary>
    /// Вид оборудования
    /// </summary>
    public class SortOfMachine
    {
        [Key]
        [Required]
        public int SortOfMachineID { get; set; }
        /// <summary>
        /// Название оборудования
        /// </summary>
        [Required]

        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        public virtual ICollection<GroupOfMachine> Groups { get; set; }
    }
}
