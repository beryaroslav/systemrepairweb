﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SystemRepairWeb.Models
{
    /// <summary>
    /// Группа оборудования
    /// </summary>
    public class GroupOfMachine
    {
        [Key]
        [Required]
        public int GroupOfMachineID { get; set; }
        /// <summary>
        /// Название
        /// </summary>
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }
        public int SortOfMachineID { get; set; }
        /// <summary>
        /// Вид оборудования
        /// </summary>
        //[Required]
        public virtual SortOfMachine Sort { get; set; }

        public virtual ICollection<TypeOfMachine> Types { get; set; }
    }
}
