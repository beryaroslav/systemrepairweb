﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SystemRepairWeb.Models
{
    public class CycleOfRepair
    {
        [Key]
        [Required]
        public int CycleOfRepairID { get; set; }

        public int GroupOfMachineID { get; set; }
        public int TypeOfRepairID { get; set; }
        public int Number { get; set; }
        public virtual GroupOfMachine Group { get; set; }
        public virtual TypeOfRepair TypeOfRepair { get; set; }
        
    }
}
