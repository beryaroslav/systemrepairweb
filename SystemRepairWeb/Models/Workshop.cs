﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SystemRepairWeb.Models
{
    public class Workshop
    {
        [Key]
        [Required]
        public int WorkshopID { get; set; }
        [Required]
        //[StringLength(50)]
        public int Number { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<Machine> Machines { get; set; }

        public string WorkshopNameNumber
        {
            get { return String.Format("{0} {1}",Number,Name); }
        }
    }
}
