﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SystemRepairWeb.Models;

namespace SystemRepairWeb.Controllers
{
    public class ModelOfMachinesController : Controller
    {
        private readonly ApplicationContext _context;

        public ModelOfMachinesController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: ModelOfMachines
        public async Task<IActionResult> Index(string searchString, string CNCcontrol)
        {
            var models = from m in _context.ModelOfMachines//.Include(m => m.Type)
                                    // orderby m.Type.Name
                select m;
            if (!String.IsNullOrEmpty(searchString))
            {
                try
                {
                    models = models.Where(s =>
                        s.Type.Name.Contains(searchString)
                        || s.Name.Contains(searchString));
                }
                catch
                {

                }
            }
            if (!String.IsNullOrEmpty(CNCcontrol))
            {
                switch (CNCcontrol)
                {
                    case "Есть":
                    {
                        models = models.Where(s => !String.IsNullOrEmpty(s.ModelCNC));
                        break;
                    }

                    case "Нет":
                    {
                        models = models.Where(s => String.IsNullOrEmpty(s.ModelCNC));
                        break;
                    }

                    case "Все":
                    {
                        models = models.Where(s => String.IsNullOrEmpty(s.ModelCNC)|| !String.IsNullOrEmpty(s.ModelCNC));
                        break;
                    }
                }
            }
            return View(await models.ToListAsync());
        }

        // GET: ModelOfMachines/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modelOfMachine = await _context.ModelOfMachines
                //.Include(m => m.Type)
                //.Include(m => m.Machines)
                .FirstOrDefaultAsync(m => m.ModelOfMachineID == id);

            if (modelOfMachine == null)
            {
                return NotFound();
            }

            return View(modelOfMachine);
        }

        // GET: ModelOfMachines/Create
        public IActionResult Create()
        {
            ViewData["TypeOfMachineID"] = new SelectList(_context.TypeOfMachines, "TypeOfMachineID", "Name");
            return View();
        }

        // POST: ModelOfMachines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ModelOfMachineID,Name,TypeOfMachineID,ModelCNC,Repairability,Country")] ModelOfMachine modelOfMachine)
        {
            if (ModelState.IsValid)
            {
                _context.Add(modelOfMachine);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["TypeOfMachineID"] = new SelectList(_context.TypeOfMachines, "TypeOfMachineID", "Name", modelOfMachine.TypeOfMachineID);
            return View(modelOfMachine);
        }

        // GET: ModelOfMachines/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modelOfMachine = await _context.ModelOfMachines.FindAsync(id);
            if (modelOfMachine == null)
            {
                return NotFound();
            }
            ViewData["TypeOfMachineID"] = new SelectList(_context.TypeOfMachines, "TypeOfMachineID", "Name", modelOfMachine.TypeOfMachineID);
            return View(modelOfMachine);
        }

        // POST: ModelOfMachines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ModelOfMachineID,Name,TypeOfMachineID,ModelCNC,Repairability,Country")] ModelOfMachine modelOfMachine)
        {
            if (id != modelOfMachine.ModelOfMachineID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(modelOfMachine);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ModelOfMachineExists(modelOfMachine.ModelOfMachineID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["TypeOfMachineID"] = new SelectList(_context.TypeOfMachines, "TypeOfMachineID", "Name", modelOfMachine.TypeOfMachineID);
            return View(modelOfMachine);
        }

        // GET: ModelOfMachines/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modelOfMachine = await _context.ModelOfMachines
                //.Include(m => m.Type)
                .FirstOrDefaultAsync(m => m.ModelOfMachineID == id);
            if (modelOfMachine == null)
            {
                return NotFound();
            }

            return View(modelOfMachine);
        }

        // POST: ModelOfMachines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var modelOfMachine = await _context.ModelOfMachines.FindAsync(id);
            _context.ModelOfMachines.Remove(modelOfMachine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ModelOfMachineExists(int id)
        {
            return _context.ModelOfMachines.Any(e => e.ModelOfMachineID == id);
        }
    }
}
