﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SystemRepairWeb.Models;

namespace SystemRepairWeb.Controllers
{
    public class CoolantsController : Controller
    {
        private readonly ApplicationContext _context;

        public CoolantsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: Coolants
        public async Task<IActionResult> Index()
        {
            return View(await _context.Coolants.ToListAsync());
        }

        // GET: Coolants/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var coolant = await _context.Coolants
                .FirstOrDefaultAsync(m => m.CoolantID == id);
            if (coolant == null)
            {
                return NotFound();
            }

            return View(coolant);
        }

        // GET: Coolants/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Coolants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CoolantID,Name")] Coolant coolant)
        {
            if (ModelState.IsValid)
            {
                _context.Add(coolant);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(coolant);
        }

        // GET: Coolants/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var coolant = await _context.Coolants.FindAsync(id);
            if (coolant == null)
            {
                return NotFound();
            }
            return View(coolant);
        }

        // POST: Coolants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CoolantID,Name")] Coolant coolant)
        {
            if (id != coolant.CoolantID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(coolant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CoolantExists(coolant.CoolantID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(coolant);
        }

        // GET: Coolants/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var coolant = await _context.Coolants
                .FirstOrDefaultAsync(m => m.CoolantID == id);
            if (coolant == null)
            {
                return NotFound();
            }

            return View(coolant);
        }

        // POST: Coolants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var coolant = await _context.Coolants.FindAsync(id);
            _context.Coolants.Remove(coolant);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CoolantExists(int id)
        {
            return _context.Coolants.Any(e => e.CoolantID == id);
        }
    }
}
