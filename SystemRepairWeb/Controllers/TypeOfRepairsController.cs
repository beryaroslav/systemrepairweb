﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SystemRepairWeb.Models;

namespace SystemRepairWeb.Controllers
{
    public class TypeOfRepairsController : Controller
    {
        private readonly ApplicationContext _context;

        public TypeOfRepairsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: TypeOfRepairs
        public async Task<IActionResult> Index()
        {
            return View(await _context.TypeOfRepair.ToListAsync());
        }

        // GET: TypeOfRepairs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeOfRepair = await _context.TypeOfRepair
                .FirstOrDefaultAsync(m => m.TypeOfRepairID == id);
            if (typeOfRepair == null)
            {
                return NotFound();
            }

            return View(typeOfRepair);
        }

        // GET: TypeOfRepairs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TypeOfRepairs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TypeOfRepairID,Name")] TypeOfRepair typeOfRepair)
        {
            if (ModelState.IsValid)
            {
                _context.Add(typeOfRepair);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(typeOfRepair);
        }

        // GET: TypeOfRepairs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeOfRepair = await _context.TypeOfRepair.FindAsync(id);
            if (typeOfRepair == null)
            {
                return NotFound();
            }
            return View(typeOfRepair);
        }

        // POST: TypeOfRepairs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TypeOfRepairID,Name")] TypeOfRepair typeOfRepair)
        {
            if (id != typeOfRepair.TypeOfRepairID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(typeOfRepair);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TypeOfRepairExists(typeOfRepair.TypeOfRepairID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(typeOfRepair);
        }

        // GET: TypeOfRepairs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeOfRepair = await _context.TypeOfRepair
                .FirstOrDefaultAsync(m => m.TypeOfRepairID == id);
            if (typeOfRepair == null)
            {
                return NotFound();
            }

            return View(typeOfRepair);
        }

        // POST: TypeOfRepairs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var typeOfRepair = await _context.TypeOfRepair.FindAsync(id);
            _context.TypeOfRepair.Remove(typeOfRepair);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TypeOfRepairExists(int id)
        {
            return _context.TypeOfRepair.Any(e => e.TypeOfRepairID == id);
        }
    }
}
