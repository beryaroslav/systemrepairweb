﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SystemRepairWeb.Models;

namespace SystemRepairWeb.Controllers
{
    public class GroupOfMachinesController : Controller
    {
        private readonly ApplicationContext _context;

        public GroupOfMachinesController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: GroupOfMachines
        public async Task<IActionResult> Index()
        {
            var applicationContext = _context.GroupOfMachines//.Include(g => g.Sort)
                                                             ;
            return View(await applicationContext.ToListAsync());
        }

        // GET: GroupOfMachines/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var groupOfMachine = await _context.GroupOfMachines
                //.Include(g => g.Sort)
                .FirstOrDefaultAsync(m => m.GroupOfMachineID == id);
            if (groupOfMachine == null)
            {
                return NotFound();
            }

            return View(groupOfMachine);
        }

        // GET: GroupOfMachines/Create
        public IActionResult Create()
        {
            ViewData["SortOfMachineID"] = new SelectList(_context.SortOfMachines, "SortOfMachineID", "Name");
            return View();
        }

        // POST: GroupOfMachines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GroupOfMachineID,Name,SortOfMachineID")] GroupOfMachine groupOfMachine)
        {
            if (ModelState.IsValid)
            {
                _context.Add(groupOfMachine);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["SortOfMachineID"] = new SelectList(_context.SortOfMachines, "SortOfMachineID", "Name", groupOfMachine.SortOfMachineID);
            return View(groupOfMachine);
        }

        // GET: GroupOfMachines/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var groupOfMachine = await _context.GroupOfMachines.FindAsync(id);
            if (groupOfMachine == null)
            {
                return NotFound();
            }
            ViewData["SortOfMachineID"] = new SelectList(_context.SortOfMachines, "SortOfMachineID", "Name", groupOfMachine.SortOfMachineID);
            return View(groupOfMachine);
        }

        // POST: GroupOfMachines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("GroupOfMachineID,Nam,SortOfMachineID")] GroupOfMachine groupOfMachine)
        {
            if (id != groupOfMachine.GroupOfMachineID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(groupOfMachine);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GroupOfMachineExists(groupOfMachine.GroupOfMachineID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["SortOfMachineID"] = new SelectList(_context.SortOfMachines, "SortOfMachineID", "Name", groupOfMachine.SortOfMachineID);
            return View(groupOfMachine);
        }

        // GET: GroupOfMachines/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var groupOfMachine = await _context.GroupOfMachines
                //.Include(g => g.Sort)
                .FirstOrDefaultAsync(m => m.GroupOfMachineID == id);
            if (groupOfMachine == null)
            {
                return NotFound();
            }

            return View(groupOfMachine);
        }

        // POST: GroupOfMachines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var groupOfMachine = await _context.GroupOfMachines.FindAsync(id);
            _context.GroupOfMachines.Remove(groupOfMachine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GroupOfMachineExists(int id)
        {
            return _context.GroupOfMachines.Any(e => e.GroupOfMachineID == id);
        }
    }
}
