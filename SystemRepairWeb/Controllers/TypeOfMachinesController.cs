﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SystemRepairWeb.Models;

namespace SystemRepairWeb.Controllers
{
    public class TypeOfMachinesController : Controller
    {
        private readonly ApplicationContext _context;

        public TypeOfMachinesController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: TypeOfMachines
        public async Task<IActionResult> Index()
        {
            var applicationContext = _context.TypeOfMachines
            //.Include(t => t.Group)
            //.Include(t => t.Group.Sort)
            ;
            return View(await applicationContext.ToListAsync());
        }

        // GET: TypeOfMachines/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeOfMachine = await _context.TypeOfMachines
                //.Include(t => t.Group)
                .FirstOrDefaultAsync(m => m.TypeOfMachineID == id);
            if (typeOfMachine == null)
            {
                return NotFound();
            }

            return View(typeOfMachine);
        }

        // GET: TypeOfMachines/Create
        public IActionResult Create()
        {
            ViewData["GroupOfMachineID"] = new SelectList(_context.GroupOfMachines, "GroupOfMachineID", "Name");
            return View();
        }

        // POST: TypeOfMachines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TypeOfMachineID,Name,GroupOfMachineID")] TypeOfMachine typeOfMachine)
        {
            if (ModelState.IsValid)
            {
                _context.Add(typeOfMachine);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupOfMachineID"] = new SelectList(_context.GroupOfMachines, "GroupOfMachineID", "Name", typeOfMachine.GroupOfMachineID);
            return View(typeOfMachine);
        }

        // GET: TypeOfMachines/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeOfMachine = await _context.TypeOfMachines.FindAsync(id);
            if (typeOfMachine == null)
            {
                return NotFound();
            }
            ViewData["GroupOfMachineID"] = new SelectList(_context.GroupOfMachines, "GroupOfMachineID", "Name", typeOfMachine.GroupOfMachineID);
            return View(typeOfMachine);
        }

        // POST: TypeOfMachines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TypeOfMachineID,Name,GroupOfMachineID")] TypeOfMachine typeOfMachine)
        {
            if (id != typeOfMachine.TypeOfMachineID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(typeOfMachine);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TypeOfMachineExists(typeOfMachine.TypeOfMachineID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupOfMachineID"] = new SelectList(_context.GroupOfMachines, "GroupOfMachineID", "Name", typeOfMachine.GroupOfMachineID);
            return View(typeOfMachine);
        }

        // GET: TypeOfMachines/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeOfMachine = await _context.TypeOfMachines
                //.Include(t => t.Group)
                .FirstOrDefaultAsync(m => m.TypeOfMachineID == id);
            if (typeOfMachine == null)
            {
                return NotFound();
            }

            return View(typeOfMachine);
        }

        // POST: TypeOfMachines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var typeOfMachine = await _context.TypeOfMachines.FindAsync(id);
            _context.TypeOfMachines.Remove(typeOfMachine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TypeOfMachineExists(int id)
        {
            return _context.TypeOfMachines.Any(e => e.TypeOfMachineID == id);
        }
    }
}
