﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SystemRepairWeb.Models;

namespace SystemRepairWeb.Controllers
{
    public class MachinesController : Controller
    {
        private readonly ApplicationContext _context;

        public MachinesController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: Machines
        public async Task<IActionResult> Index(string sortOrder, string searchString,string repairControl)
        {

            ViewData["ModelSortParm"] = String.IsNullOrEmpty(sortOrder) ? "model_desc" : "";
            ViewData["DateSortParm"] = sortOrder == "DateStart" ? "date_desc" : "DateStart";
           
            var machines = from s in _context.Machines
                 select s;
            if (!String.IsNullOrEmpty(searchString) )
            {
                try
                {
                    machines = machines.Where(s =>
                    s.InventoryNumber.Contains(searchString)
                                                   || s.Model.Name.Contains(searchString));
                }
                catch
                {

                }
            }

            if (!String.IsNullOrEmpty(repairControl))
            {
                switch (repairControl)
                {
                    case "Есть":
                    {
                        machines = machines.Where(s => s.Repairs.Count > 0);
                        break;
                    }

                    case "Нет":
                    {
                        machines = machines.Where(s => s.Repairs.Count == 0);
                        break;
                    }

                    case "Все":
                    {
                        machines = machines.Where(s => s.Repairs.Count >= 0);
                        break;
                    }
                }
            }
            switch (sortOrder)
            {
                case "model_desc":
                    machines = machines.OrderByDescending(s => s.Model.Name);
                    break;
                case "DateSortParm":
                    machines = machines.OrderBy(s => s.DateStart);
                    break;
                case "date_desc":
                    machines = machines.OrderByDescending(s => s.DateStart);
                    break;
                default:
                    machines = machines.OrderBy(s => s.Model.Name);
                    break;
            }
            return View(await machines/*.AsNoTracking()*/.ToListAsync());
         //   return View(await applicationContext.ToListAsync());
        }

        // GET: Machines/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var machine = await _context.Machines
                .FirstOrDefaultAsync(m => m.MachineID == id);
            if (machine == null)
            {
                return NotFound();
            }

            return View(machine);
        }

        // GET: Machines/Create
        public IActionResult Create()
        {
            ViewData["ModelOfMachineID"] = new SelectList(_context.ModelOfMachines, "ModelOfMachineID", "Name");
            ViewData["WorkshopID"] = new SelectList(_context.Workshops, "WorkshopID", "WorkshopNameNumber");
            return View();
        }

        // POST: Machines/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MachineID,ModelOfMachineID,WorkshopID,InventoryNumber,DateStart")] Machine machine)
        {
            if (ModelState.IsValid)
            {
                var inv = await _context.Machines
                    .FirstOrDefaultAsync(m => m.InventoryNumber == machine.InventoryNumber);
                if (inv == null)
                {
                    _context.Add(machine);
                    await _context.SaveChangesAsync();


                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    ViewData["ModelOfMachineID"] = new SelectList(_context.ModelOfMachines, "ModelOfMachineID", "Name", machine.ModelOfMachineID);
                    ViewData["WorkshopID"] = new SelectList(_context.Workshops, "WorkshopID", "WorkshopNameNumber", machine.WorkshopID);

                    return View(machine);
                }
            }
            ViewData["ModelOfMachineID"] = new SelectList(_context.ModelOfMachines, "ModelOfMachineID", "Name", machine.ModelOfMachineID);
            ViewData["WorkshopID"] = new SelectList(_context.Workshops, "WorkshopID", "WorkshopNameNumber", machine.WorkshopID);
            return View(machine);
        }

        // GET: Machines/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var machine = await _context.Machines.FindAsync(id);
            if (machine == null)
            {
                return NotFound();
            }
            ViewData["ModelOfMachineID"] = new SelectList(_context.ModelOfMachines, "ModelOfMachineID", "Name", machine.ModelOfMachineID);
            ViewData["WorkshopID"] = new SelectList(_context.Workshops, "WorkshopID", "WorkshopNameNumber", machine.WorkshopID);
            return View(machine);
        }

        // POST: Machines/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MachineID,ModelOfMachineID,WorkshopID,InventoryNumber,DateStart")] Machine machine)
        {
            if (id != machine.MachineID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var inv = await _context.Machines
                        .FirstOrDefaultAsync(m => m.InventoryNumber == machine.InventoryNumber);
                    if (inv == null)
                    {
                        _context.Update(machine);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        ViewData["ModelOfMachineID"] = new SelectList(_context.ModelOfMachines, "ModelOfMachineID", "Name", machine.ModelOfMachineID);
                        ViewData["WorkshopID"] = new SelectList(_context.Workshops, "WorkshopID", "WorkshopNameNumber", machine.WorkshopID);
                        return View(machine);
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MachineExists(machine.MachineID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ModelOfMachineID"] = new SelectList(_context.ModelOfMachines, "ModelOfMachineID", "Name", machine.ModelOfMachineID);
            ViewData["WorkshopID"] = new SelectList(_context.Workshops, "WorkshopID", "WorkshopNameNumber", machine.WorkshopID);
            return View(machine);
        }

        // GET: Machines/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var machine = await _context.Machines
                .FirstOrDefaultAsync(m => m.MachineID == id);
            if (machine == null)
            {
                return NotFound();
            }

            return View(machine);
        }

        // POST: Machines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var machine = await _context.Machines.FindAsync(id);
            
            _context.Machines.Remove(machine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MachineExists(int id)
        {
            return _context.Machines.Any(e => e.MachineID == id);
        }

        // GET: Machines/Generate/5
        public async Task<IActionResult> Generate(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var machine = await _context.Machines.FindAsync(id);
            if (machine == null)
            {
                return NotFound();
            }
            return View(machine);
        }

        public void DateOfWeekControl(DateTime date)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                {
                    date = date.AddDays(2);
                    break;
                }
                case DayOfWeek.Sunday:
                {
                    date = date.AddDays(1);
                    break;
                }
            }
            
        }
        // POST: Machines/Generate/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Generate(int? id, DateTime start,DateTime finish)
        {
            var machine = await _context.Machines.FindAsync(id);
            DateTime date = start.Date;
            var rep1 = await _context.TypeOfRepair.FirstOrDefaultAsync(m => m.Name == "Осмотр");
            var rep2 = await _context.TypeOfRepair.FirstOrDefaultAsync(m => m.Name == "Малый ремонт");
            List<TypeOfRepair> Cycle = new List<TypeOfRepair>() {rep1, rep1, rep1, rep2};

            while (date<=finish)
            {
                foreach (var item in Cycle)
                {
                    if (date <= finish)
                    {
                        List<MonthlyAllotment> monthlyAllotments = await _context.MonthlyAllotment
                            .Where(ma => ma.Month.Year == date.Year)
                            .ToListAsync();
                        DateTime dateBuf = date.Date;
                        foreach (var month in monthlyAllotments)
                        {
                            ///Поиск подходящего месяца
                            if (month.Month.Month == dateBuf.Month)
                            {
                                double sumRepairability = machine.Model.Repairability;
                                ///Подсчет фактической ремонтосложности текущего месяца
                                foreach (var sumRep in month.Repairs)
                                {
                                    sumRepairability += sumRep.Machine.Model.Repairability;
                                }
                                ///Если фактическая ремонтосложность превышает планируюмую
                                if (sumRepairability > month.MonthlyRepairability)
                                {
                                    dateBuf = dateBuf.AddMonths(1);
                                    continue;
                                }
                                else
                                {
                                    ///Проверка изменения даты
                                    if (dateBuf.Date == date.Date)
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        date = dateBuf.Date;
                                        break;
                                    }
                                }
                               
                            }
                        }
                        

                        switch (date.DayOfWeek)
                        {
                            case DayOfWeek.Saturday:
                            {
                                Repair rep = new Repair
                                    {Machine = machine, DateStart = date.Date.AddDays(2), Type = item};
                                MonthlyAllotment monthlyAllotment = await _context.MonthlyAllotment
                                    .FirstOrDefaultAsync(ma => ma.Month.Month == rep.DateStart.Month
                                                               && ma.Month.Year == rep.DateStart.Year);
                                if (monthlyAllotment != null)
                                {
                                    rep.MonthlyAllotmentID = monthlyAllotment.MonthlyAllotmentID;
                                    rep.Month = monthlyAllotment;
                                }
                                    _context.Repairs.Add(rep);
                                break;
                            }
                            case DayOfWeek.Sunday:
                            {
                                Repair rep = new Repair
                                    {Machine = machine, DateStart = date.Date.AddDays(1), Type = item};
                                MonthlyAllotment monthlyAllotment = await _context.MonthlyAllotment
                                    .FirstOrDefaultAsync(ma => ma.Month.Month == rep.DateStart.Month
                                                               && ma.Month.Year == rep.DateStart.Year);
                                if (monthlyAllotment != null)
                                {
                                    rep.MonthlyAllotmentID = monthlyAllotment.MonthlyAllotmentID;
                                    rep.Month = monthlyAllotment;
                                }
                                    _context.Repairs.Add(rep);
                                break;
                            }
                            default:
                            {
                                Repair rep = new Repair
                                    { Machine = machine, DateStart = date.Date, Type = item };
                                MonthlyAllotment monthlyAllotment = await _context.MonthlyAllotment
                                    .FirstOrDefaultAsync(ma => ma.Month.Month == rep.DateStart.Month
                                                               && ma.Month.Year == rep.DateStart.Year);
                                if (monthlyAllotment != null)
                                {
                                    rep.MonthlyAllotmentID = monthlyAllotment.MonthlyAllotmentID;
                                    rep.Month = monthlyAllotment;
                                }
                                    _context.Repairs.Add(rep);
                                    break;
                            }
                        }
                        date = date.AddMonths(6);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
