﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SystemRepairWeb.Models;

namespace SystemRepairWeb.Controllers
{
    public class CycleOfRepairsController : Controller
    {
        private readonly ApplicationContext _context;

        public CycleOfRepairsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: CycleOfRepairs
        public async Task<IActionResult> Index()
        {
            var applicationContext = _context.CycleOfRepairs//.Include(c => c.Group)
                                                            ;
            return View(await applicationContext.ToListAsync());
        }

        // GET: CycleOfRepairs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cycleOfRepair = await _context.CycleOfRepairs
                //.Include(c => c.Group)
                .FirstOrDefaultAsync(m => m.CycleOfRepairID == id);
            if (cycleOfRepair == null)
            {
                return NotFound();
            }

            return View(cycleOfRepair);
        }

        // GET: CycleOfRepairs/Create
        public IActionResult Create()
        {
            ViewData["GroupOfMachineID"] = new SelectList(_context.GroupOfMachines, "GroupOfMachineID", "Name");
            return View();
        }

        // POST: CycleOfRepairs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CycleOfRepairID,GroupOfMachineID")] CycleOfRepair cycleOfRepair)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cycleOfRepair);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupOfMachineID"] = new SelectList(_context.GroupOfMachines, "GroupOfMachineID", "Name", cycleOfRepair.GroupOfMachineID);
            return View(cycleOfRepair);
        }

        // GET: CycleOfRepairs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cycleOfRepair = await _context.CycleOfRepairs.FindAsync(id);
            if (cycleOfRepair == null)
            {
                return NotFound();
            }
            ViewData["GroupOfMachineID"] = new SelectList(_context.GroupOfMachines, "GroupOfMachineID", "Name", cycleOfRepair.GroupOfMachineID);
            return View(cycleOfRepair);
        }

        // POST: CycleOfRepairs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CycleOfRepairID,GroupOfMachineID")] CycleOfRepair cycleOfRepair)
        {
            if (id != cycleOfRepair.CycleOfRepairID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cycleOfRepair);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CycleOfRepairExists(cycleOfRepair.CycleOfRepairID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupOfMachineID"] = new SelectList(_context.GroupOfMachines, "GroupOfMachineID", "Name", cycleOfRepair.GroupOfMachineID);
            return View(cycleOfRepair);
        }

        // GET: CycleOfRepairs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cycleOfRepair = await _context.CycleOfRepairs
                //.Include(c => c.Group)
                .FirstOrDefaultAsync(m => m.CycleOfRepairID == id);
            if (cycleOfRepair == null)
            {
                return NotFound();
            }

            return View(cycleOfRepair);
        }

        // POST: CycleOfRepairs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cycleOfRepair = await _context.CycleOfRepairs.FindAsync(id);
            _context.CycleOfRepairs.Remove(cycleOfRepair);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CycleOfRepairExists(int id)
        {
            return _context.CycleOfRepairs.Any(e => e.CycleOfRepairID == id);
        }
    }
}
