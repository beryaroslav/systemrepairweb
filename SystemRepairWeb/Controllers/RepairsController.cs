﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SystemRepairWeb.Models;

namespace SystemRepairWeb.Controllers
{
    public class RepairsController : Controller
    {
        private readonly ApplicationContext _context;

        public RepairsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: Repairs
        public async Task<IActionResult> Index(string typeOfRepair,string workshop, string searchString, DateTime? startDate, DateTime? endDate )
        {
            IQueryable<string> typesRepairQuery = from m in _context.Repairs
                orderby m.Type.Name
                select m.Type.Name;
            IQueryable<string> workshopQuery = from w in _context.Workshops
                orderby w.WorkshopNameNumber
                select w.WorkshopNameNumber;
                 var repairs = from s in _context.Repairs
            select s;

            if (!String.IsNullOrEmpty(searchString))
            {
                try
                {
                    repairs = repairs.Where(s =>
                            s.Machine.InventoryNumber.Contains(searchString)
                            || s.Type.Name.Contains(searchString)
                            || s.DateStart.ToString("{0:yyyy-MM-dd}").Contains(searchString)
                            || s.DateFinish.ToString("{0:yyyy-MM-dd}").Contains(searchString)
                    );
                }
                catch
                {

                }
            }

            if (!string.IsNullOrEmpty(typeOfRepair))
            {
                repairs = repairs.Where(x => x.Type.Name == typeOfRepair);
            }

            if (!String.IsNullOrEmpty(workshop))
            {
                repairs = repairs.Where(o => o.Machine.Workshop.WorkshopNameNumber == workshop);
            }
            if (startDate != null && endDate != null && (startDate <= endDate || endDate==DateTime.MinValue))
            {
                repairs = repairs.Where(r => (r.DateStart >= startDate )
                                             && ( r.DateStart <= endDate));
            }
            else
            if (startDate != null)
            {
                
                repairs = repairs.Where(y => y.DateStart >= startDate );
            }
            else
            if (endDate != null)
            {
                repairs = repairs.Where(z => z.DateStart<=endDate);
            }
            var typeRepairVM = new RepairTypeViewModel
            {
                Types = new SelectList(await typesRepairQuery.Distinct().ToListAsync()),
                Workshops = new SelectList(await workshopQuery.Distinct().ToListAsync()),
                Repairs = await repairs.ToListAsync()
            };
            return View(typeRepairVM);
        }

        // GET: Repairs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var repair = await _context.Repairs
                .FirstOrDefaultAsync(m => m.RepairID == id);
            if (repair == null)
            {
                return NotFound();
            }

            return View(repair);
        }

        // GET: Repairs/Create
        public IActionResult Create()
        {
            ViewData["MachineID"] = new SelectList(_context.Machines, "MachineID", "NameNumber");
            ViewData["TypeOfRepairID"] = new SelectList(_context.TypeOfRepair, "TypeOfRepairID", "Name");
            return View();
        }

        // POST: Repairs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RepairID,DateStart,DateFinish,MachineID,TypeOfRepairID,Note")] Repair repair)
        {
            if (ModelState.IsValid)
            {
                MonthlyAllotment monthlyAllotment = await _context.MonthlyAllotment
                    .FirstOrDefaultAsync(ma => ma.Month.Month == repair.DateStart.Month
                                               && ma.Month.Year == repair.DateStart.Year);
                if (monthlyAllotment != null)
                {
                    repair.MonthlyAllotmentID = monthlyAllotment.MonthlyAllotmentID;
                    repair.Month = monthlyAllotment;
                }
                _context.Add(repair);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MachineID"] = new SelectList(_context.Machines, "MachineID", "NameNumber", repair.MachineID);
            ViewData["TypeOfRepairID"] = new SelectList(_context.TypeOfRepair, "TypeOfRepairID", "Name", repair.TypeOfRepairID);
            return View(repair);
        }

        // GET: Repairs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var repair = await _context.Repairs.FindAsync(id);
            if (repair == null)
            {
                return NotFound();
            }
            ViewData["MachineID"] = new SelectList(_context.Machines, "MachineID", "NameNumber", repair.MachineID);
            ViewData["TypeOfRepairID"] = new SelectList(_context.TypeOfRepair, "TypeOfRepairID", "Name", repair.TypeOfRepairID);
            return View(repair);
        }

        // POST: Repairs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RepairID,DateStart,DateFinish,MachineID,TypeOfRepairID,Note")] Repair repair)
        {
            if (id != repair.RepairID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    MonthlyAllotment monthlyAllotment = await _context.MonthlyAllotment
                        .FirstOrDefaultAsync(ma => ma.Month.Month == repair.DateStart.Month
                                                   && ma.Month.Year == repair.DateStart.Year);
                    if (monthlyAllotment != null)
                    {
                        repair.MonthlyAllotmentID = monthlyAllotment.MonthlyAllotmentID;
                        repair.Month = monthlyAllotment;
                    }

                    _context.Update(repair);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RepairExists(repair.RepairID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MachineID"] = new SelectList(_context.Machines, "MachineID", "NameNumber", repair.MachineID);
            ViewData["TypeOfRepairID"] = new SelectList(_context.TypeOfRepair, "TypeOfRepairID", "Name", repair.TypeOfRepairID);
            return View(repair);
        }

        // GET: Repairs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var repair = await _context.Repairs
                .FirstOrDefaultAsync(m => m.RepairID == id);
            if (repair == null)
            {
                return NotFound();
            }

            return View(repair);
        }

        // POST: Repairs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var repair = await _context.Repairs.FindAsync(id);
            _context.Repairs.Remove(repair);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        
        public IActionResult AddSparePart()
        {
            ViewData["RepairID"] = new SelectList(_context.Repairs, "RepairID", "RepairID");
            ViewData["SparePartID"] = new SelectList(_context.SpareParts, "SparePartID", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddSparePart([Bind("RepairID,SparePartID")] RepairSparePart repairSparePart)
        {
            if (ModelState.IsValid)
            {
                _context.Add(repairSparePart);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), repairSparePart.Repair.RepairID);
            }
            ViewData["RepairID"] = new SelectList(_context.Repairs, "RepairID", "RepairID", repairSparePart.Repair.RepairID);
            ViewData["SparePartID"] = new SelectList(_context.SpareParts, "SparePartID", "Name", repairSparePart.SparePart.SparePartID);
            return View(repairSparePart);
        }

        private bool RepairExists(int id)
        {
            return _context.Repairs.Any(e => e.RepairID == id);
        }
    }
}
