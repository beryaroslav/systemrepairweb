﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SystemRepairWeb.Models;

namespace SystemRepairWeb.Controllers
{
    public class MonthlyAllotmentsController : Controller
    {
        private readonly ApplicationContext _context;

        public MonthlyAllotmentsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: MonthlyAllotments
        public async Task<IActionResult> Index()
        {
            var months = from month in _context.MonthlyAllotment
                orderby month.Month
                         select month;
            return View(await months.ToListAsync());
        }

        // GET: MonthlyAllotments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var monthlyAllotment = await _context.MonthlyAllotment
                .FirstOrDefaultAsync(m => m.MonthlyAllotmentID == id);
            if (monthlyAllotment == null)
            {
                return NotFound();
            }

            return View(monthlyAllotment);
        }

        // GET: MonthlyAllotments/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MonthlyAllotments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MonthlyAllotmentID,Month,MonthlyRepairability,WorkDays")] MonthlyAllotment monthlyAllotment)
        {
            if (ModelState.IsValid)
            {
                var monthlyYear = await _context.MonthlyAllotment
                    .FirstOrDefaultAsync(m => m.Month.Month == monthlyAllotment.Month.Month);
                if (monthlyYear == null)
                {
                    _context.Add(monthlyAllotment);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View(monthlyAllotment);
                }
                
                

            }
            return View(monthlyAllotment);
        }

        // GET: MonthlyAllotments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var monthlyAllotment = await _context.MonthlyAllotment.FindAsync(id);
            if (monthlyAllotment == null)
            {
                return NotFound();
            }
            return View(monthlyAllotment);
        }

        // POST: MonthlyAllotments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MonthlyAllotmentID,Month,MonthlyRepairability,WorkDays")] MonthlyAllotment monthlyAllotment)
        {
            if (id != monthlyAllotment.MonthlyAllotmentID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                   // var monthlyYear = await _context.MonthlyAllotment
                   //     .FirstOrDefaultAsync(m => m.Month.Month == monthlyAllotment.Month.Month 
                   //                               && m.Month.Year == monthlyAllotment.Month.Year) ;
                   // if (monthlyYear == null|| monthlyYear!= monthlyAllotment)
                   // {
                        _context.Update(monthlyAllotment);
                        await _context.SaveChangesAsync();
                   // }
                   // else
                   // {
                    //    return View(monthlyAllotment);
                    //}
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MonthlyAllotmentExists(monthlyAllotment.MonthlyAllotmentID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(monthlyAllotment);
        }

        // GET: MonthlyAllotments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var monthlyAllotment = await _context.MonthlyAllotment
                .FirstOrDefaultAsync(m => m.MonthlyAllotmentID == id);
            if (monthlyAllotment == null)
            {
                return NotFound();
            }

            return View(monthlyAllotment);
        }

        // POST: MonthlyAllotments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var monthlyAllotment = await _context.MonthlyAllotment.FindAsync(id);
            _context.MonthlyAllotment.Remove(monthlyAllotment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> SelectYear()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SelectYear(DateTime year)
        {
            List<MonthlyAllotment> monthlyAllotments = await _context.MonthlyAllotment.Where(y=>y.Month.Year==year.Year).ToListAsync();
            List<Machine> machines = await _context.Machines.ToListAsync();
            double sumRepairability = 0;
            foreach (var item in machines)
            {
                sumRepairability += item.Model.Repairability;
            }

            double sumDays = 0;
            foreach (var item in monthlyAllotments)
            {
                    sumDays += item.WorkDays;
            }

            double repairabilityDay = sumRepairability / sumDays;
            foreach (var item in monthlyAllotments)
            {
                    item.MonthlyRepairability = Math.Ceiling(repairabilityDay * item.WorkDays);
            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MonthlyAllotmentExists(int id)
        {
            return _context.MonthlyAllotment.Any(e => e.MonthlyAllotmentID == id);
        }
    }
}
