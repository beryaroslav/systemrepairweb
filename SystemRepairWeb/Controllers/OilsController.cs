﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SystemRepairWeb.Models;

namespace SystemRepairWeb.Controllers
{
    public class OilsController : Controller
    {
        private readonly ApplicationContext _context;

        public OilsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: Oils
        public async Task<IActionResult> Index()
        {
            return View(await _context.Oils.ToListAsync());
        }

        // GET: Oils/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var oil = await _context.Oils
                .FirstOrDefaultAsync(m => m.OilID == id);
            if (oil == null)
            {
                return NotFound();
            }

            return View(oil);
        }

        // GET: Oils/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Oils/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OilID")] Oil oil)
        {
            if (ModelState.IsValid)
            {
                _context.Add(oil);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(oil);
        }

        // GET: Oils/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var oil = await _context.Oils.FindAsync(id);
            if (oil == null)
            {
                return NotFound();
            }
            return View(oil);
        }

        // POST: Oils/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("OilID")] Oil oil)
        {
            if (id != oil.OilID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(oil);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OilExists(oil.OilID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(oil);
        }

        // GET: Oils/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var oil = await _context.Oils
                .FirstOrDefaultAsync(m => m.OilID == id);
            if (oil == null)
            {
                return NotFound();
            }

            return View(oil);
        }

        // POST: Oils/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var oil = await _context.Oils.FindAsync(id);
            _context.Oils.Remove(oil);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OilExists(int id)
        {
            return _context.Oils.Any(e => e.OilID == id);
        }
    }
}
