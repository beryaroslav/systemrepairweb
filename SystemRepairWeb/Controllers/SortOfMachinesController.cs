﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SystemRepairWeb.Models;

namespace SystemRepairWeb.Controllers
{
    public class SortOfMachinesController : Controller
    {
        private readonly ApplicationContext _context;

        public SortOfMachinesController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: SortOfMachines
        public async Task<IActionResult> Index()
        {
            //var sorts = _context SortOfMachines.A;
            return View(await _context.SortOfMachines.ToListAsync());
        }

        // GET: SortOfMachines/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sortOfMachine = await _context.SortOfMachines
                .FirstOrDefaultAsync(m => m.SortOfMachineID == id);
            if (sortOfMachine == null)
            {
                return NotFound();
            }

            return View(sortOfMachine);
        }

        // GET: SortOfMachines/Create
        public IActionResult Create()
        {

            return View();
        }

        // POST: SortOfMachines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SortOfMachineID,Name")] SortOfMachine sortOfMachine)
        {
            if (ModelState.IsValid)
            {
                _context.Add(sortOfMachine);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(sortOfMachine);
        }

        // GET: SortOfMachines/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sortOfMachine = await _context.SortOfMachines.FindAsync(id);
            if (sortOfMachine == null)
            {
                return NotFound();
            }
            return View(sortOfMachine);
        }

        // POST: SortOfMachines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SortOfMachineID,Name")] SortOfMachine sortOfMachine)
        {
            if (id != sortOfMachine.SortOfMachineID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(sortOfMachine);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SortOfMachineExists(sortOfMachine.SortOfMachineID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(sortOfMachine);
        }

        // GET: SortOfMachines/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sortOfMachine = await _context.SortOfMachines
                .FirstOrDefaultAsync(m => m.SortOfMachineID == id);
            if (sortOfMachine == null)
            {
                return NotFound();
            }

            return View(sortOfMachine);
        }

        // POST: SortOfMachines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var sortOfMachine = await _context.SortOfMachines.FindAsync(id);
            _context.SortOfMachines.Remove(sortOfMachine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SortOfMachineExists(int id)
        {
            return _context.SortOfMachines.Any(e => e.SortOfMachineID == id);
        }
    }
}
